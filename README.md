# Linuxdeploy cli tools by Jerry (For kindle)
### See https://github.com/meefik/linuxdeploy first.
You need to figure out how linuxdeploy works before using the scripts.  
It can be used on any linux devices with busybox installed.  
Use qemu to build an image.  (on a x86 device) e.g. debian  
`sudo apt install binfmt-support qemu qemu-user-static`  
It is very simple, no documentation, so you have to rtfs.
## To clone the repository
I haven't set up git web, so you may download the source code directly.
